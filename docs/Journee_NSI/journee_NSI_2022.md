---
hide:
    - toc
---


![flyer](journee_nsi_flyer.jpg){ width=30% } 

## Programme 

### ENVIRONNEMENT DE TRAVAIL ET PRATIQUES PÉDAGOGIQUES 

- [Usage en classe des concours Castor, Algorea, France-IOI](./01_Busser_FranceIOI.pdf), **A. Busser**
- [MkDocs pour un site enseignant, exemple de e-nsi](./02_Nativel_ensi.pdf), **F. Nativel**
- [Analyse des usages et approche instrumentale](./03_Declercq_AnalyseUsages_ApprocheInstrumentale.pdf), **C. Declercq**


### DONNER DU SENS AUX APPRENTISSAGES 

- [Les thèmes Internet et Protocoles de contrôle par les activités débranchées en Seconde](./04_Esprimont_SNT.pdf) SNT, **D. Esprimont** 
- [Une animation pour découvrir des algorithmes de tri en Première NSI](./05_Nativel_algotri.pdf), **F. Nativel** 
- Labyrinthe et graphe en Terminale NSI. Une illustration avec Turtle, **B. Laval**
- [Retour sur la session _Jeux et Apprentissages_ de Didapro 9](07_Defay_DidaproSessionJeux.pdf), **N. Defaÿ** 

### TRAVAILLER EN MODE PROJET

- [Expérience de la gestion de projets en SI](./08_Esprimont_ProjetSI.pdf), **D. Esprimont**
- [Favoriser l'autonomie par l'apprentissage en mode projet en première année de Licence Informatique](./09_Hoarau_ProjetEtAutonomie.pdf), **S. Hoarau**
- [Co-conception de minis projets pour la Première NSI dans une UE d'enseignement en deuxième année de Licence Informatique](10_Declercq_DemarcheProjetNSI.pdf), **C. Declercq**


### Enseigner l'informatique au lycée : quelles perspectives donner aux élèves en terme de poursuite d'études et d'emploi ?

**Résumé de la table ronde**

L'informatique est la science du numérique. L'initiation à l'informatique en SNT permet de mieux comprendre le monde numérique, et éventuellement de donner aux élèves, filles et garçons, l'envie de prolonger la découverte de cette discipline en spécialité NSI en première, puis d'approfondir en terminale.

Mais après ? Comment le choix de la spécialité NSI est pris en compte à l'entrée dans l'enseignement supérieur ? Quelles sont les opportunités de carrières offertes par le secteur du numérique dans la Région ? Quel niveau de maîtrise du numérique et de l'informatique est demandé par ces emplois ? et par les métiers à venir ?

Des responsables de formation post-bac, des professionnel.les du secteur, d'ancien.nes étudiant.es échangent et répondent à vos questions au cours de cette table ronde

**Invité.e.s**

- **S. Chane-Lune**, Enseignante de la spécialité NSI au lycée Le Verger
- **F. Fabre Ferber**, Doctorant en Informatique au LIM, Laboratoire d'Informatique et de Mathématiques de l'UFR Sciences et Technologies
- **R. Girard**, Enseignant-chercheur en Informatique, Directeur du département Informatique de l'UFR Sciences et Technologies
- **D. Grondin**, Ingénieur en Informatique, Développeur polyvalent et auto-entrepreneur 
- **J. Mauras**, Président de ExoData groupe Informatique spécialisé dans les services _cloud_ et la cybersécurité
- **P.-U. Tournoux**, Enseignant-chercheur à l’Université de la Réunion dans la spécialité Informatique et Télécommunication de l’ESIROI



