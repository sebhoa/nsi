# Déroulé de la table ronde

## Ouverture

L'animateur :

> Après ces retours d'expériences inspirants, ces réflexions sur les pratiques pédagogiques et didactiques en Informatique, nous allons questionner le futur de nos élèves dans cette discipline. Quels débouchés à la fois en termes de poursuite d'études post bac et en matière de perspective d'emplois ?
>
> Pour alimenter cette réflexion, j'accueille :
> 
> - Sophie Chane-Lune, Enseignante de la spécialité NSI au lycée Le Verger
> - Frédérick Fabre Ferber, Doctorant en Informatique au LIM, Laboratoire d'Informatique et de Mathématiques de l'UFR Sciences et Technologies
> - Régis Girard, Enseignant-chercheur en Informatique, Directeur du département Informatique de l'UFR Sciences et Technologies
> - Djothi Grondin, Ingénieur en Informatique, Développeur polyvalent et auto-entrepreneur 
> - Julien Mauras, Président de ExoData groupe Informatique spécialisé dans les services _cloud_ et la cybersécurité
> - Pierre-Ugo Tournoux, Enseignant-chercheur à l’Université de la Réunion dans la spécialité Informatique et Télécommunication de l’ESIROI

## Première question 30'

> Nous pouvons peut-être commencer par une première question pour laquelle je propose que chacun vienne apporter son éclairage en 3-4 minutes puis nous prendrons quelques questions de la salle et des personnes présentent à distance. 

QUESTION :

> Lorsque l'on observe les choix effectués par les jeunes au moment d'abandonner une de leur spécialité, lorsqu'on discute avec les parents, on se rend compte que l'Informatique a du mal à se faire connaitre comme une vraie discipline aussi sérieuse que les mathématiques, la physique, les sciences de la vie, la médecine, le droit etc. Comment rassurer les jeunes et leurs parents ? 

## Deuxième question 30'

> Sur le même principe que pour notre première question, pouvez-vous nous donner votre réaction concernant ...

QUESTION :

> 