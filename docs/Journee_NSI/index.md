---
hide:
    - toc
---

## Journée Académique NSI

Organisée conjointement par l'Académie de La Réunion, l'UFR Sciences et Technologies de l'Université de la Réunion et l'Institut National Supérieur du Professorat et de l'Education, cette journée se veut un temps d'échanges entre :

- enseignant.e.s de NSI / SNT
- universitaires informaticien.ne.s et didacticien.ne.s
- professionnel.le.s de la filière numérique
- étudiant.e.s 

## Partenaires

![Rectorat](logo_ac_small.jpg) ![Logo UR](logo_ur.jpg) ![Inspe](logo_inspe.jpg){ width="250px" } ![Logo FST](logo_ufr_st.png){ width="180px" } 

![logo CAI](logo_cai.svg){ width="180px" } ![Logo Europe](Flag_of_Europe.svg){ width="100px" }

