---
hide:
    - navigation
    - toc
---


## Bienvenue sur cette page NSI

Vous trouverez ici notamment le site de la journée académique NSI co-organisée par L'UFR Sciences de l'Université de la Réunion, L'Académie de la Réunion et l'Institut National Supérieur du Professorat et de l'Éducation
