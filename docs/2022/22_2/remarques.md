Une erreur de frappe dans l'énoncé. On préfèrera `n` à `N`. Le principal souci de cet exercice est la question explicite sur la fonction `range` avec 3 paramètres. Clairement hors-programme. Il faut au contraire donner ce que fait `range`, par exemple au moment où on parle des multiples `2*i`, `3*i` etc.

Dans l'énoncé modifié, on rajoute des trous et des exemples.