Écrire une fonction `indice` qui prend en paramètres `element` un nombre entier, `tableau` un tableau de nombres entiers, et qui renvoie l'indice de la première occurrence de `element` dans `tableau` (et `None` s'il en est absent).

Exemples :
```python
>>> indice(1, [10, 12, 1, 56])
2
>>> indice(1, [1, 50, 1])
0
>>> indice(15, [8, 9, 10, 15])
3
>>> indice(1, [2, 3, 4]) is None
True
```