N. Bonnin :

Beaucoup (trop?) de compétences testées dans cet exercice : 
+ dictionnaire avec maniement d’une double indexation clé/indice de liste
+ récursivité
+ algorithme de parcours
+ compréhension d’une modélisation différente de celle préconisée dans le programme

Cela fait beaucoup pour un exercice 1 .

Source : [commentaires à propos du 27.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26))