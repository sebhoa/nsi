Écrire une fonction `tri_selection` qui prend en paramètre une tableau `tab` de nombres
entiers et qui trie ce tableau **en place** (c'est-à-dire que le tableau est modifié) par ordre croissant des valeurs.

On utilisera l’algorithme suivant :

- On parcourt le tableau de gauche à droite :
    - on recherche le minimum du tableau entre cette position courante et la fin du tableau 
    - on échange alors les 2 valeurs

Exemple :
```python
>>> tab = [1, 52, 6, -9, 12]
>>> tri_selection(tab)
>>> tab
[-9, 1, 6, 12, 52]
>>> tab_vide = []
>>> tri_selection(tab_vide)
>>> tab_vide
[]
``` 