Commentaire de S. Hoarau :

- L'algorithme n'est pas décrit correctement : il ne s'agit pas de chercher le deuxième plus petit, puis le 3e plus petit etc. ce qui peut sembler difficile à réaliser. L'algorithme s'énonce comme ceci :
    - On parcourt le tableau de gauche à droite et on recherche le minimum du tableau entre cette position courante et la fin du tableau 
    - On échange alors les 2 valeurs
- Il s'agit d'un _tri en place_ ... pourquoi renvoyer le tableau ? Il faudrait au moins un mot d'explication (s'il s'agit par exemple de simplifier l'exemple). (Edit : nous sommes plusieurs à penser que ce n'est pas une justification suffisante)

Mais le tri par sélection reste d'un niveau bien supérieur à ce qui est proposé en général en exercice 1. 
