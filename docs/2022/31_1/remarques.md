R. Janvier :

- Encore une fonction recherche… C’est pour embrouiller les élèves qui apprennent par coeur les solutions sans rien comprendre ?
- Le sujet se restreint aux données numériques, mais pourquoi ? On pourrait donner des exemples avec des textes. Il manque un exemple ou la valeur cherchée est en première position, au cas où.

Source : [commentaires à propos du 31.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62)


Effectivement il s'agit d'une fonction `nb_occurrences`  qui fonctionne pas uniquement avec des valeurs numériques.