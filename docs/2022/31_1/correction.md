```python linenums='1'
def nb_occurrences(element, tableau):
    nb_apparition = 0
    for elt in tableau:
        if element == elt:
            nb_apparition += 1
    return nb_apparition
```

Bien sûr on s'expose à cette réponse :

```python
def nb_occurrences(element, tableau):
    return tableau.count(element)
```