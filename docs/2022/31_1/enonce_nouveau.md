Écrire en langage Python une fonction `nb_occurrences` prenant comme paramètres une valeur `element`
et un tableau `tableau` (type `list`) et qui renvoie le nombre d'occurrences de `element` dans `tableau`.

Exemples :
```python
>>> nb_occurrences(5, [])
0
>>> nb_occurrences(5, [5, 3, 4, 8])
1
>>> nb_occurrences('a', ['b', 'a', 'a', 'a', 'c'])
3
>>> nb_occurrences(True, [False, False, True])
1
```