Recopier et compléter la fonction suivante en respectant la spécification. On
ne recopiera pas les commentaires.

```python linenums='1'
def dichotomie(tab, x):
    """
    tab : tableau d’entiers trié dans l’ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = ...
    fin = ...
    while debut <= fin:
        milieu = ...
        if x == tab[milieu]:
            return ...
        if x > ...:
            debut = ...
        else:
            fin = ...
    return ...
```

Exemples :
```python
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],28)
True
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],27)
False
>>> dichotomie([], 1)
False
```