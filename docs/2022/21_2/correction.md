```python linenums='1'
def dichotomie(tab, x):
    """
    tab : tableau d’entiers trié dans l’ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        milieu = (debut + fin) // 2
        if x == tab[milieu]:
            return True
        if x > tab[milieu]:
            debut = milieu + 1
        else:
            fin = milieu - 1
    return False
```