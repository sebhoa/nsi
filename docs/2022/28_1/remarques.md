Pas de grosses fautes mais comme pour les x exemplaires de recherche d'extremum, le calcul de la moyenne d'un tableau de valeurs est un peu éculé et pas très _sexy_ comme exercice. Ici le tableau est bien défini non vide donc pas de souci de division par zéro à traiter.

Comme le fait  remarquer N. Bonnin : pourquoi préciser qu'il s'agit de flottants ?