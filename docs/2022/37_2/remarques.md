Commentaire de S. Hoarau :

- bon exercice de manipulation de listes et dictionnaire
- pas besoin de la variable `vainqueur`  lors de la recherche du nombre de voix le plus élevé (d'autant que la fonction s'appelle déjà comme ça)
- on peut augmenter un peu la quantité de zones à compléter