G. Connan semble décontenancé par cet énoncé, dès le nom de la fonction, qui cache ce qu'elle réalise vraiment :

> Tiens, encore une recherche...Sauf que c'est  une toute autre recherche que dans
le sujet 1. On peut donc l'appeler recherche_consecutifs peut-être.
Ensuite la liste  en paramètre est désignée  comme étant un tableau  et la liste
renvoyée est  désignée comme étant une  liste. Comme M. Preskovic,  je suis dans
toutes mes confuses avec un tel énoncé.

Source : [commentaires à propos du 04.1](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-41)