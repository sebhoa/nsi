R. Janvier :

- Il y a 2 paramètres : la somme à rendre et ce qui est déjà rendu. Mais quel intérêt ? Pour une fonction récursive, pourquoi pas ?
- On met tout en centimes, mais pourquoi ne pas juste rester sur les euros ? Là aussi, on complexifie pour pas grand chose.
- Et puis les valeurs sont dans l’ordre croissant. Donc il faut partir de la fin de la liste.
- Il manque des exemples où on utilise plusieurs fois la même pièce.
- Passable mais compliqué pour pas grand chose. À simplifier

Source : [commentaires à propos du 31.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62)

L'exercice a été donné en sujet 1. La version corrigée va simplement reprendre celle de ce sujet.