R. Janvier relève qu'une fonction qui renvoie un objet d'un type différent en cas d'erreur ce n'est pas une bonne idée. Il propose de considérer le tableau de l'exercice comme non vide pour éliminer l'erreur de la division par zéro.

Il se pose aussi la question de l'interdiction d'utiliser la fonction prédéfinie `sum`. Notons enfin que cet exercice se retrouve en 35.1 et 39.1.