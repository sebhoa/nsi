La fonction `tri_bulles` prend en paramètre un tableau `tab` d’entiers et effectue un tri **en place** du tableau par ordre croissant de ses éléments suivant le principe suivant :

- on parcourt le tableau de droite à gauche (ie du dernier indice au premier) et on va _pousser_ le plus grand élément à cette position courante :
    - on parcourt le tableau de gauche à droite jusqu'à cette position et on compare l'élément courant avec son suivant ; on les échange s'ils sont mal ordonnés

Compléter le code Python ci-dessous qui implémente la fonction `tri_bulles` (et une fonction `echange`  pour échanger deux valeurs d'un tableau).

```python linenums='1'
def echange(tab, i, j):
    ...

def tri_bulles(tab):
    n = len(tab)
    for i in range(..., ..., -1):
        for j in range(i):
            if tab[j] > tab[...]:
                echange(..., ..., ...)
```


Exemples :

```python
>>> tableau = [5, 3, 6, 1, 4, 2]
>>> tri_bulles(tableau)
>>> tableau
[1, 2, 3, 4, 5, 6]
```


