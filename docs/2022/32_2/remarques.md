R. Janvier [à propos du 32.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62) :

* Bof. 
* L'idée de départ peut être intéressante, mais comme on n'incrémente pas sur l'avant dernier octet, il n'y a rien à faire. 
* En plus il manque des tests montrant ce qui se passe s'il n'y a pas d'adresse valide suivante. 
* La seule difficulté, c'est la manipulation entre les `int` et les `str`. 
* Il faudrait permettre de passer de 192.168.0.254 à 192.168.1.1, par exemple. 
* Ou alors rajouter quelque chose de plus fin sur les masques de réseau en passant en binaire. 
* Sans intérêt