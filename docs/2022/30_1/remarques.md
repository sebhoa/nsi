N. Bonnin pense (à juste titre) que cet exercice est trop difficile pour un exercice 1. Effectivement la fusion de listes est donné en exercice 2 du sujet 10.

Source : [commentaires à propos du 30.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26)

Nous ne proposons donc rien pour cet exercice qui doit être supprimé des exerices 1.