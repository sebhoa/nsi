N. Bonnin :

Algorithme de tri sélection en place, décrit assez longuement dans l’énoncé alors que c’est un algorithme au programme), quel contraste avec l’exercice 2 du 26. C’est une bonne intention, mais…

+ C’est un tri en place mais on retourne le tableau
+ Il y a une coquille dans le résultat affiché (le « tableau trié » n’est pas dans l’ordre)

Source : [commentaires à propos du 27.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26))

Bon il ne s'agit pas vraiment d'un tri par sélection puisque le parcourt du tableau se fait par la droite. De plus, le test à la sortie de la boucle interne pour éviter de faire l'échange est superflu. Comme pou d'autres exercices, on préfèrera utiliser une fonction `echange`  pour l'échange de valeurs d'un tableau : chaque candidat-e utilisant alors l'implémentation de son choix (affectation multiple de Python, utilisation d'une variable intermédiaire etc.).