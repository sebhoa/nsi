On considère l'algorithme de tri de tableau suivant : à chaque étape, on parcourt depuis
le début du tableau tous les éléments non rangés et on place en dernière position le plus
grand élément.

Exemple avec le tableau : `t = [41, 55, 21, 18, 12, 6, 25]`

- Étape 1 : on parcourt tous les éléments du tableau, on permute le plus grand élément avec le dernier.

    Le tableau devient `t = [41, 25, 21, 18, 12, 6, 55]`

- Étape 2 : on parcourt tous les éléments **sauf le dernier**, on permute le plus grand élément trouvé avec l'avant dernier.

    Le tableau devient : ```t = [6, 25, 21, 18, 12, 41, 55]``` 

Et ainsi de suite. La code de la fonction `tri_iteratif` qui implémente cet algorithme est donné ci-
dessous ; ainsi que la fonction `echange` qui permet d'échanger 2 valeurs d'un tableau en fournissant les indices :

```python linenums='1'
def echange(tab, i, j):
    ...

def tri_iteratif(tab):
    for k in range(..., 0, -1):
        indice_max = ...
        for i in range(0, ...):
            if tab[i] > ... :
                indice_max = i
        if tab[indice_max] > ...:
            echange(..., ..., ...)
```

Compléter les deux fonctions pour finaliser ce tri et tester :

```python
>>> T = [41, 55, 21, 18, 12, 6, 25]
>>> tri_iteratif(T)
>>> T
[6, 12, 18, 21, 25, 41, 55]
``` 

