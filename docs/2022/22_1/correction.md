Énoncé très simple mais le solutions sont nombreuses :

```python
def renverse(mot):
    tom = ''
    for caractere in mot:
        tom = caractere + tom
    return tom
```

Une version récursive

```python
def renverse(mot):
    if mot == '':
        return ''
    else:
        return renverse(mot[1:]) + mot[0]
```

En utilisant le _slicing_ (**hors-programme**) :

```python
def renverse(mot):
    return mot[::-1]
```

