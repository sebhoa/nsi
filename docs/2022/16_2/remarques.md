D'après Q. Konieczko, ce deuxième exercice souffre de :

- mauvais nommage de variables,
- présence inutile d'un `print`

Source : [commentaires à propos du 16.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12)

Je rajouterai :

- une utilisation bien fastidieuse de la pile... on se demande pourquoi avoir choisi cette structure, pas du tout adapté au problème.
- l'affectation `T2 = []` à la fin du `while` qui vient de vider `T2` est inutile 
- de même que la variable intermédiaire `x` dans la deuxième boucle
- on peut rappeler que lors de l'affichage en ligne d'une pile, la droite de la pile représente le sommet
- un _s_ au nom de la fonction, on recupère tous les positifs
