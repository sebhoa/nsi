R. Janvier :

- Encore une fonction recherche…
- Sinon, ça change un peu.
- Il manque un exemple où la dernière occurrence est à la position 0.
- Ok

Source : [commentaires à propos du 32.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62)

Certains ne sont pas ok pour la valeur -1 comme valeur de retour en cas d'absence de l'élément recherché. C'est pourtant ce que retourne la fonction Python `find`. Là je pense que les avis seront partagés, en fonction des habitudes des un-es et des autres et surtout des pratiques avecles élèves.
