F. Chambon nous livre ses [commentaires à propos du 06.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/14) :

- mauvais nom de fonctions
- le cas de la liste vide n'est pas évoqué
- problème de PEP8 dans l'exemple

Il s'agit du même exercice que le 16.1. On y appliquera donc les mêmes modifications.