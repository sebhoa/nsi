```python
def maxliste(tab):
    valeur_max = 0
    for valeur in tab:
        if valeur > valeur_max:
            valeur_max = valeur
    return valeur_max
```

On risque de voir :

```python
def maxliste(tab):
    return max(tab)
```

