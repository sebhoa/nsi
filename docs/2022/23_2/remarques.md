C'est un bon exercice, qui utilise une structure de Pile dont on donne l'implémentation. Le candidat n'a pas à coder cette structure, simplement l'utiliser.

Pas d'énoncé modifié. On pourrait donner quelques exemples de plus (surtout que l'exemple donné n'est pas celui de l'énoncé) et dire qu'on suppose toujours les expressions bien formées (pas de gestion d'erreur).

Exemples :

```python
>>> eval_expression([3, 2, '*', 5, '+']) #correspond à 3 * 2 + 5, l'ex. de l'énoncé
11
>>> eval_expression([2, 3, '+', 5, '*']) #correspond à (2 + 3) * 5
25
```
