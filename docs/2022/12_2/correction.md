```python linenums='1'
def separe(tab):
    """place tous les 0 de tab à gauche et tous les 1 à droite"""
    debut = 0  # indice de début
    fin = len(tab) - 1    # indice de fin
    while debut < fin:
        if tab[debut] == 0:
            debut += 1
        else:
            tab[debut] = tab[fin]
            tab[fin] = 1
            fin -= 1 
```

Les pointillés sont mis pour que l'élève puisse écrire cette version :

```python linenums='1'
def separe(tab):
    """place tous les 0 de tab à gauche et tous les 1 à droite"""
    debut = 0  # indice de début
    fin = len(tab) - 1    # indice de fin
    while debut < fin:
        if tab[debut] == 0:
            debut = debut + 1
        else:
            tab[debut] = tab[fin]
            tab[fin] = 1
            fin = fin - 1 
```
