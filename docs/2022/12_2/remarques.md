R. Janvier dit apprécier cet exercice, qu'on retrouve dans le sujet 26, moins bien expliqué d'après lui. Il soulève quelques soucis de typographie (des espaces manquantes) ainsi que ce problème quasi récurrent sur toutes les fonctions de tri en place : cette volonté de faire tout de même un `return` pour faciliter l'affichage des exemples.

Le nom de la version sujet 26 (`separe`) est plus sympathique aussi. Ce dernier sujet s'autorise l'échange de valeurs par affectation multiple. Mais cette solution ne peut pas être exigée. La solution serait donc de proposer au candidat d'écrire une fonction `echange(tab, i, j)`. 

F. Nativel remarque, très justement :

> Puisqu'on sait que le tableau ne contient que des 0 ou des 1, dans le `else` on sait qu'il s'agit d'un 1. La conséquence : on peut se passer de l'échange et faire uniquement deux affectations :

```python
        ...
        else:
            tab[debut] = tab[fin]
            tab[fin] = 1
        ...
```

