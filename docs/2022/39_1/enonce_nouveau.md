Écrire une fonction `moyenne` prenant en paramètres un tableau non vide d’entiers et qui renvoie la
moyenne des valeurs du tableau.

Exemple :
```python
>>> moyenne([10, 20, 30, 40, 60, 110])
45.0
>>> moyenne([1, 3])
2.0
```