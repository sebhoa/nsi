```python linenums='1'
def moyenne(tableau):
    somme = 0
    for valeur in tableau:
        somme += valeur
    return somme / len(tableau)
```

On trouvera sûrement :

```python
def moyenne(tableau):
    return sum(tableau) / len(tableau)
```