Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers
`a` et `b`, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction. 

On pourra s'inspirer de la méthode suivante :

1. on calcule la multiplication des valeurs positives de `a` et `b` (attention en ne se servant que de l'addition)
2. on applique la règle des signes : si `a` et `b` sont de même signe alors le résultat de la multiplication est positif sinon il est négatif.

On pourra se servir de la fonction `abs`  qui donne la baleur absolue d'un nombre :

```python
>>> abs(5)
5
>>> abs(-5)
5
```

**Version sans fonction `abs`**

Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers
`a` et `b`, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction. 

On pourra utiliser l'indication suivante : $(-a)\times (-b) = a\times b$


