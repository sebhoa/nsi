Une première version issue de l'énoncé original, et en se servant de la règle : $5 \\times (-6)= - (5 \\times 6)$.

```python linenums='1'
def multiplication(a, b):
    if a < 0:
        return -multiplication(-a, b)
    if b < 0:
        return -multiplication(a, -b)
    produit = 0
    for _ in range(b):
        produit += a
    return produit
```

La version avec `abs` :

```python
def multiplication(a, b):
    produit = 0
    absolue_b = abs(b)
    for _ in range(abs(a)):
        produit += absolue_b
    if (a > 0 and b < 0) or (a < 0 and b > 0): 
        return -produit
    else:
        return produit
```

La version sans `abs` :

```python
def multiplication(a, b):
    if a < 0:
        a, b = -a, -b
    produit = 0
    for _ in range(a):
        produit += b
    return produit
```

et avec un itérateur :

```python
def multiplication(a, b):
    if a < 0:
        a, b = -a, -b
    return sum(b for _ in range(a))
```