Lorsque j'ai lu ce sujet pour la première fois, je me suis dis que l'auteur avait du oublier de préciser que les entiers étaient positifs. Puis j'ai vu que la soustraction était autorisé donc je me suis dis que non on avait bien à traiter les 4 cas.

F. Nativel a une remarque intéressante sur le sujet : 

> Les élèves apprennent au collège « je multiplie les nombres sans leur signe et j’applique la règle des signes ». Ce même algo peut servir de guide pour un énoncé plus abordable.

J. Diraison propose une autre idée : se passer de la valeur absolue et  utiliser le fait que $(-a)\times (-b) = a\times b$.

Nommage des variables : pour une opérations arithmétique, `a` et `b` semble acceptable comme noms de variables, en tout cas pas plus mauvais que `n1`, `n2`.

Dans tous les cas cet exercice reste difficile pour un exercice 1 (a priori pas vraiment un algorithme vu).
