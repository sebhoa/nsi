Commentaires de R. Janvier :

Sur le principe c'est intéressant, mais je le trouve hyper dur pour les élèves faibles. 
Il faudrait donner les fonctions `nbCol` et `nbLig`. La dernière est introuvable pour certains élèves plus faibles. On utilise peu les tableaux en 2D en Tle (les graphes ne sont pas au programme du bac) et donc c'est le genre de détails que les élèves oublient. 
Contentons nous de la manipulation de `image[i][j]` sans demander la hauteur et surtout la largeur. 
En plus dans `negatif` on utilise `len(image)` au lieu de `nbLig(image)`. 
Ils ont corrigé une erreur qu'il y avait dans le sujet de l'année dernière dans la fonction `binaire` et rajouté une précision sur le calcul du négatif. 
Par contre l'exemple de `binaire` est faux puisqu'il correspond au négatif et pas à `img`. 
Et il y a une valeur supérieure à 255 dans l'exemple. 
Je ne parle pas du nom des fonctions... 
Problématique. 
La partie intéressante est très facile alors que les fonctions annexes sont potentiellement trop difficiles. 
Il vaudrait mieux rajouter une fonction pour le miroir horizontal ou vertical, qui demande de la réflexion plus que des connaissances.
J'ai modifié un peu l'énoncé, mais on voit bien que c'est 2 fois la même chose. Il manque vraiment un travail à faire sur les indices.

[commentaire à propos du 34.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62#exercice-2-manipulation-dune-image-en-niveaux-de-gris-15)
