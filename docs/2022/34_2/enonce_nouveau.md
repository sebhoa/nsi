On considère une image en 256 niveaux de gris que l’on représente par une grille de
nombres, c’est-à-dire une liste composée de sous-listes toutes de longueurs identiques.

La largeur de l’image est donc la longueur d’une sous-liste et la hauteur de l’image est le
nombre de sous-listes.

Chaque sous-liste représente une ligne de l’image et chaque élément des sous-listes est
un entier compris entre 0 et 255, représentant l’intensité lumineuse du pixel.

Le négatif d’une image est l’image constituée des pixels `x_n` tels que `x_n + x_i = 255`
où `x_i` est le pixel correspondant de l’image initiale.

Compléter les fonctions ci-dessous :

```python linenums='1'
def nb_lignes(image):
    '''renvoie le nombre de lignes de l'image'''
    return len(image)

def nb_colonnes(image):
    '''renvoie la largeur de l'image'''
    return len(image[0])

def negatif(image):
    '''renvoie le négatif de l'image sous la forme d'une liste de listes'''
    # on créé une image de 0 aux mêmes dimensions que le paramètre image
    nouvelle_image = [[0 for k in range(...)] for i in range(...)] 
    for i in range(...):
        for j in range(...):
            nouvelle_image[i][j] = ...
    return nouvelle_image

def binaire(image, seuil):
    '''renvoie une image binarisée de l'image sous la forme
    d'une liste de listes contenant des 0 si la valeur
    du pixel est strictement inférieure au seuil
    et 1 sinon'''
    # on crée une image de 0 aux mêmes dimensions que le paramètre image
    nouvelle_image = [[0 for k in range(...)] for i in range(...)]
    for i in range(...):
        for j in range(...):
            if ...:
                nouvelle_image[i][j] = ...
            else:
                nouvelle_image[i][j] = ...
    return nouvelle_image    
```

Exemple :
```python
>>> img = [[20, 34, 254, 145, 6], [23, 124, 217, 225, 69], [197, 174, 207, 25, 87], [255, 0, 24, 197, 189]]
>>> nbLig(img)
4
>>> nbCol(img)
5
>>> negatif(img)
[[235, 221, 1, 110, 249], [232, 131, 38, 30, 186], [58, 81, 48, 230, 168], [0, 255, 231, 58, 66]]
>>> binaire(negatif(img), 120)
[[0, 0, 1, 1, 0], [0, 0, 1, 1, 0], [1, 1, 1, 0, 0], [1, 0, 0, 1, 1]]
```
