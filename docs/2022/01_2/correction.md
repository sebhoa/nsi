G. Connan propose un certain nombre de corrections sur sa page.

La version attendue (sans la valeur par défaut) et en mettant les espaces correctement :

```python linenums='1'
pieces = [100,50,20,10,5,2,1]

def rendu_glouton(arendre, solution, i):
    if arendre == 0:
        return solution
    p = pieces[i]
    if p <= arendre:
        solution.append(p)
        return rendu_glouton(arendre - p, solution, i)
    else:
        return rendu_glouton(arendre, solution, i+1)
```

Sachant que cette fonction s'appelle **toujours** avec la liste vide en deuxième paramètre et 0 a priori en 3e... sauf si on sait qu'on n'a plus de billets de 100.

G. Connan ne détaille pas celle avec la fonction auxiliaire , et pour cause : la fonction en question n'a aucun sens hors la fonction principale et devrait donc être définie à l'intérieur de cette-dernière ce qui fait un code complètement **hors-programme** :

```python
def rendu_glouton(a_rendre):
    
    def rendu_glouton_aux(a_rendre, solution, i):
        if a_rendre == 0:
            return solution
        piece = pieces[i]
        if piece <= a_rendre:
            solution.append(piece)
            return rendu_glouton_aux(a_rendre - piece, solution, i)
        else:
            return rendu_glouton_aux(a_rendre, solution, i+1)

    pieces = [100, 50, 20, 10, 5, 2, 1]
    return rendu_glouton_aux(a_rendre, [], 0)
``` 

Version plus générique, avec la liste des pièces en paramètre :

```python
def rendu_glouton(a_rendre, pieces):
    
    def rendu_glouton_aux(a_rendre, solution, i):
        if a_rendre == 0:
            return solution
        elif i >= len(pieces):
            return None
        else:
            piece = pieces[i]
            if piece <= a_rendre:
                solution.append(piece)
                return rendu_glouton_aux(a_rendre - piece, solution, i)
            else:
                return rendu_glouton_aux(a_rendre, solution, i+1)

    return rendu_glouton_aux(a_rendre, [], 0)
``` 

Exemples :

```python
>>> rendu_glouton(291, [100, 50, 20, 10, 5, 2, 1])
[100, 100, 50, 20, 20, 1]
>>> rendu_glouton(291, [50, 20, 10, 2, 1])
[50, 50, 50, 50, 50, 20, 20, 1]
>>> rendu_glouton(291, [50, 20, 10, 2])
None
```