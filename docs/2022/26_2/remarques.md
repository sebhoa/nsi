N. Bonnin nous dit :

+ L’énoncé est un peu lapidaire.
+ Le return `tab` final est superflu.

Source : [commentaires à propos du 26.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26)

Cet exercice est le même que le 12.2, la version modifiée sera donc celle proposée à cet exercice.
