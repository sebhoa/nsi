Q. Konieczko nous a proposé une relecture de ce sujet 16. Il souligne le nombre de tests trop peu nombreux et l'absence de spécification en cas de tableau vide.

Source : [commentairesà propos du 16.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12)

Pour moi l'exercice est surtout beaucoup trop simple. L'exercice 2 malheureusement ne sauvera pas ce sujet.

_A noter_ : il s'agit du même exercice que le 06.1, où F. Chambon suggère de choisir un nom un peu plus explicite pour la fonction.