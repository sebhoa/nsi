Dans cet exercice, on appelle carré d’ordre $n$ un tableau de $n$ lignes et $n$ colonnes dont chaque case contient un entier naturel.

Exemples :

![image](assets/images/20_2/img20_2.png){: .center width=70%}

Un carré est dit magique lorsque les sommes des éléments situés sur chaque ligne, chaque
colonne et chaque diagonale sont égales. Ainsi c2 et c3 sont magiques car la somme de chaque
ligne, chaque colonne et chaque diagonale est égale à 2 pour c2 et 15 pour c3. c4 n’est pas
magique car la somme de la première ligne est égale à 34 alors que celle de la dernière colonne
est égale à 27.

La classe `Carre` ci-après contient des méthodes qui permettent de manipuler des carrés.

Compléter la fonction `est_magique` qui prend en paramètre un carré et qui renvoie `True` si le carré est magique, `False` sinon.

```python linenums='1'
class Carre:

    def __init__(self, tableau):
        self.ordre = len(tableau)
        self.valeurs = tableau

    def somme_ligne(self, i):
        '''Calcule la somme des valeurs de la ligne i'''
        return sum(self.valeurs[i])

    def somme_col(self, j):
        '''Calcule la somme des valeurs de la colonne j'''
        return sum([self.valeurs[i][j] for i in range(self.ordre)])

def est_magique(carre):
    n = carre.ordre
    s = carre.somme_ligne(0)

    #test de la somme de chaque ligne
    for i in range(..., ...):
        if carre.somme_ligne(i) != s:
            return ...

    #test de la somme de chaque colonne
    for j in range(n):
        if ... != s:
            return False

    #test de la somme de chaque diagonale
    if sum([carre.valeurs[...][...] for k in range(n)]) != s:
        return False
    if sum([carre.valeurs[k][n-1-k] for k in range(n)]) != s:
        return False
    return ...
```

On vous donne les tableaux de valeurs des carrés de la figure :

```python
>>> TAB2 = [[1, 1], [1, 1]]
>>> TAB3 = [[2, 9, 4], [7, 5, 3], [6, 1, 8]]
>>> TAB4 = [[4, 5, 16, 9], [14, 7, 2, 11], [3, 10, 15, 6], [13, 12, 8, 1]]
``` 

Compléter les instructions ci-dessous pour modéliser les carrés correspondants :

```python
>>> C2 = ...(TAB2)
>>> C3 = ...
>>> C4 = ...
```

Tester la fonction `est_magique` sur les carrés :

```python
>>> est_magique(C2)
True
>>> est_magique(C3)
True
>>> est_magique(C4)
False
```
