Il y a une virgule en trop dans `'o,'`.
On oblige à utiliser des tableaux alors que les dictionnaires sont plus pratiques pour cela. 
On ne rappelle pas l'usage de `index` qui est indispensable avec les tableaux. 
En fait on a un peu 2 exercices en 1 : compter les occurrences et chercher le max. 
Ok, on peut faire les 2 en même temps, mais quand dans d'autres sujets on ne demande de traiter qu'un seul de ces problèmes, cela pique un peu.
Surtout que pour compter les occurrences avec les tableaux, c'est bien plus compliqué, surtout pour les élèves en difficulté.
À la rigueur, on pourrait donner le tableau des occurrences et demander le lettre qui apparaît le plus. Ou juste demander le tableau des occurrences. 
Problématique.

[commentaire à propos du 34.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62#sujet-34-13)
