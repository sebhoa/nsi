S. Hoarau :

- Faire des tests d'égalité sur des `float` n'a aucun sens : le `assert` pour tester la fonction `distance` est donc à revoir.
- Il manque quelque chose pour ne pas inciter à calculer deux fois la distance courante... on pourrait aussi nommé `meilleur` ou `plus_proche`  le point le plus proche et ` point`le point courant.

Modulo ces changements c'est un bon exercice.
