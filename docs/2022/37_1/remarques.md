S. Hoarau :

Énoncé clair et simple. Un exercice qui se situe au bon niveau de difficulté pour un exercice 1. Éventuellement la fonction pourrait s'appeler `est_trie`, le préfixe `est_` rappelant qu'il s'agit d'une fonction booléenne. Pourquoi imposer que le tableau est non vide ? 