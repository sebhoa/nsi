```python
def est_trie(tableau):
    for i in range(len(tableau)-1):
        if tableau[i] > tableau[i+1]:
            return False
    return True
```

Pour ceux et celles qui n'aiment pas les multiples `return` ou qui aiment bien les `while` :

```python
def est_trie(tableau):
    correctement_ordonne = True
    i = 0
    while correctement_ordonne and i < len(tableau)-1:
        correctement_ordonne = tab[i] <= tab[i+1]
        i += 1
    return correctement_ordonne
```