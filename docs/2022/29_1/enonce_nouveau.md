On s’intéresse à la suite d’entiers définie ainsi :

- les deux premiers termes sont 1 et 1
- chacun des suivants est obtenu par la somme des deux termes précédents

Elle s’appelle la suite de Fibonacci.

Écrire la fonction `fibonacci` qui prend un entier `n > 0` et qui renvoie l’élément d’indice
`n` de cette suite.

Exemples :

```python
>>> fibonacci(1)
1
>>> fibonacci(2)
1
>>> fibonacci(25)
75025
>>> fibonacci(45)
1134903170
```