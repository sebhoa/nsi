La suite de Fibonacci est un grand classique donc même si les suites ne sont pas connues des candidat-es ne faisant pas de mathématiques en spécialité, on peut s'attendre à ce que toutes et tous connaissent cette suite là.

Néanmoins on peut en faire une définition sans le formalisme de suite définie par récurrence.

D'autre part, la mention "On utilisera une programmation dynamique (pas de récursivité)" laisse perplexe. L'auteur-e du sujet voulait probablement dire : on utilisera une solution itérative.

Comme déjà mentionné, soit on propose un exercice 2, à trous parce qu'on a en tête un algorithme ; soit on propose un exercice 1 mais alors pourquoi tenter d'imposer une méthode de résolution ? Peut-être que le candidat ou la candidate aura vu la suite de Fibonacci justement dans le cadre de la récursivité, avec mémoïzation par exemple. 