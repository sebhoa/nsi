```python
def expression_infixe(e):
    if e.est_une_feuille():
        return str(e)
    else:
        return '(' + expression_infixe(e.gauche) + e.valeur + expression_infixe(e.droit) + ')'
```

La version avec une f-_string_ est nettement mieux :

```python
def expression_infixe(e):
    if e.est_une_feuille():
        return str(e)
    else:
        return f'({expression_infixe(e.gauche)}{e.valeur}{expression_infixe(e.droit)})'
```
