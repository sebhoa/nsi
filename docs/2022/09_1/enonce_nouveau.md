Soit un nombre entier $n$ supérieur ou égal à 1 :

- si $n$ est pair, on le divise par 2 ;
- si $n$ est impair, on le multiplie par 3 et on ajoute 1.

Puis on recommence ces étapes avec le nombre entier obtenu, jusqu’à ce que l’on obtienne la valeur 1.

La suite des nombres ainsi obtenue se nomme _suite de Syracuse de_ $n$. Une conjecture dit que cette suite se termine toujours par 1.

Écrire une fonction `syracuse` prenant en paramètre un entier `n` strictement positif et qui renvoie la liste des valeurs de la suite, en partant de `n` et jusqu'à atteindre 1.

Exemple :
```python
>>> syracuse(7)
[7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
>>> syracuse(1)
[1]
>>> syracuse(4)
[4, 2, 1]
```