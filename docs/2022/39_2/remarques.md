S. Hoarau :

- Harmoniser le nommage des fonctions, en respectant le PEP8 : `zoomListe`  et `zoomDessin`  devraient être `zoom_liste` et `zoom_dessin` resp. 
- Pas besoin du caractère `\` lorsqu'on veut créer une structure type `list`, `tuple`, `dict`... sur plusieurs lignes
- Pour la fonction affiche on aurait apprécié une solution un peu plus _intelligente_ avec l'utilisation d'une constante `CARACTERES = ['  ', ' *']`
- Aucune raison que la variable pour la deuxième boucle se nomme `col` : il ne s'agit pas d'une colonne mais d'une valeur binaire sur la ligne...
- dans la fonction `zoomDessin` la variable de la première boucle devrait s'appeler `ligne` 

**Autre version de la fonction `affiche`**

```python
CARACTERES = ['  ', ' *']

def affiche(dessin):
    for ligne in dessin:
        for digit in ligne:
            print(CARACTERES[digit], end="")
        print()
```