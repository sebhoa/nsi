N. Bonnin note que l'énoncé pourrait être un peu plus propre au niveau de la formulation (on parle d'additionner des nombres et des caractères). Il y a aussi un regret de ne pas voir plus de valeurs données pour le dictionnaire des association chiffres romains - décimaux.

Il manque des exemples.

Source : [commentaires à propos du 30.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26)

Une maladresse notable : le dictionnaire qui devrait être une constante est définie à l'intérieur d'une fonction récursive. Cela signifie qu'à chaque appel ce dictionnaire sera recréé. A corriger absolument.

