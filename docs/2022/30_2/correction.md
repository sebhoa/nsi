```python
ROMAIN_DEC = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}

def rom_to_dec(nombre):
    """ Renvoie l’écriture décimale du nombre donné en chiffres romains """
    if len(nombre) == 1:
        return ROMAIN_DEC[nombre]
    else:
        # on supprime le premier caractère de la chaîne contenue dans la variable nombre
        # et cette nouvelle chaîne est enregistrée dans la variable nombre_droite
        nombre_droite = nombre[1:]
        if ROMAIN_DEC[nombre[0]] >= ROMAIN_DEC[nombre[1]]:
            return ROMAIN_DEC[nombre[0]] + rom_to_dec(nombre_droite)
        else:
            return rom_to_dec(nombre_droite) - ROMAIN_DEC[nombre[0]] 
```