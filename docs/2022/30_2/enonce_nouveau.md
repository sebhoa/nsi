Les chiffres romains sont un système ancien d’écriture des nombres.

Les chiffres romains sont: I, V, X, L, C, D, et M. Ces symboles représentent respectivement 1, 5, 10, 50, 100, 500, et 1000 en base dix.

Cette association pourra être modélisée par un dictionnaire défini une fois pour toute (une constante). Vous complèterez le dictionnaire dont on vous donne le début ci-dessous :

```python
ROMAIN_DEC = {"I":1, "V":5, "X":10, "L":50, ...}
```

Lorsque deux caractères successifs sont tels que le caractère placé à gauche possède une
valeur supérieure ou égale à celui de droite, le nombre s’obtient en additionnant la valeur du caractère de gauche à la valeur de la chaîne située à droite.

Ainsi, "XVI" est le nombre 16 (10 + 6) car X vaut 10 > à 5 qui est la valeur de V.

Lorsque deux caractères successifs sont tels que le caractère placé à gauche possède une
valeur strictement inférieure à celui de droite, le nombre s’obtient en retranchant la valeur du caractère de gauche à la valeur de la chaîne située à droite.


Ainsi, "CDIII" est le nombre 403 (503 - 100) car 100 (la valeur de C) est inférieure strictement à 500 (la valeur de D).


On souhaite créer une fonction récursive `rom_to_dec` qui prend en paramètre une chaîne de
caractères (non vide) représentant un nombre écrit en chiffres romains et renvoyant le nombre
associé en écriture décimale :

```python linenums='1'
def rom_to_dec (nombre):
    """ Renvoie l’écriture décimale du nombre donné en chiffres romains """
    if len(nombre) == 1:
        return ...
    else:
        # on supprime le premier caractère de la chaîne contenue dans la variable nombre
        # et cette nouvelle chaîne est enregistrée dans la variable nombre_droite
        nombre_droite = nombre[1:] 
        if ROMAIN_DEC[...] >= ...:
            return ROMAIN_DEC[nombre[0]] + ...
        else:
            return ...

assert rom_to_dec("CXLII") == 142
assert rom_to_dec("XVI") == 16
assert om_to_dec("CDIII") == 403
```