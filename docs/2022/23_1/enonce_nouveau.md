Sur le réseau social TipTop, on s’intéresse au nombre de « like » des abonnés.
Les données sont stockées dans des dictionnaires où les clés sont les pseudos et les valeurs
correspondantes sont les nombres de « like » comme ci-dessous :

`{'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}`


Écrire une fonction `top_like` qui :

- Prend en paramètre un dictionnaire `likes` non vide dont les clés sont des chaînes de
caractères et les valeurs associées sont des entiers ;
- Renvoie un tuple dont :
    - La première valeur est la clé du dictionnaire associée à la valeur maximale ; en cas d'égalité sur plusieurs clés, on choisira la plus petite suivant l'ordre alphabétique
    - La seconde valeur est la valeur maximale présente dans le dictionnaire.

Exemples :

```python
>>> top_like({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50})
('Ada', 201)
>>> top_like({'Alan': 222, 'Ada': 201, 'Eve': 222, 'Tim': 50})
('Alan', 222)
```