```python
def top_like(likes):
    top_pseudo = None
    top_nb_likes = 0
    for pseudo in likes:
        nb_likes = likes[pseudo]
        if nb_likes > top_nb_likes or nb_likes == top_nb_likes and pseudo < top_pseudo:
            top_pseudo = pseudo
            top_nb_likes = nb_likes
    return top_pseudo, top_nb_likes
```

En utilisant la méthode `items`

```python
def top_like(likes):
    top_pseudo = None
    top_nb_likes = 0
    for pseudo, nb_likes in likes.items():
        if nb_likes > top_nb_likes or nb_likes == top_nb_likes and pseudo < top_pseudo:
            top_pseudo = pseudo
            top_nb_likes = nb_likes
    return top_pseudo, top_nb_likes
```

