S. Hoarau s'interroge sur quelques uns des choix de l'énoncé de cet exercice 2 :

- Un double dictionnaire est peut-être d'un niveau un peu élevé ; ne serait-ce que vis à vis de la moyenne des exercices donnés. 
- pourquoi faire une itération sur `values()`  pour le 2e dictionnaire ? pas sûr que se soit exigé comme connaissance
- pourquoi le dictionnaire de gestion des notes n'est-il pas un paramètre de la fonction ? 
