L’occurrence d’un caractère dans une phrase est son apparition dans la phrase.

Exemples :

- dans `'bonjour'` il y a 2 occurrences du caractère `'o'` ;
- dans `'Bébé'` il y a une seule occurrence du caractère `'b'` et une seule du caractère `'B'` ;
- on compte 3 occurrences du caractère `' '` (espace) dans `'Bonjour le monde !'`.

On cherche à comptabiliser les occurrences des caractères dans une phrase. On souhaite stocker ces
nombres d'occurrences dans un dictionnaire dont les clefs seraient les caractères de la phrase et
les valeurs associées les nombres d'occurrences de ces caractères.

Écrire une fonction `nb_occurrences` prenant comme paramètre une chaîne de caractères
`phrase` et qui renvoie le dictionnaire des nombres d'occurrences. 

Exemples : 

```python
>>> nb_occurrences('Bonjour !')
{'B': 1, 'o': 2, 'n': 1, 'j': 1, 'u': 1, 'r': 1, ' ': 1, '!': 1}
>>> nb_occurrences('ha ! ha ! ha !')
{'h': 3, 'a': 3, ' ': 5, '!': 3}
```
