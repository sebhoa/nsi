N. Bonnin s'est attelée à la relecture des sujets 26 à 30. Elle pointe l'erreur de nommage de la fonction qui ne respecte pas le PEP8 et pose la question de l'utilisation conjointe de `index` et `min`  pour arriver au résultat.

Il faudrait préciser aussi que le tableau n'est pas vide.

Source : [commentaires à propos du 26.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26)