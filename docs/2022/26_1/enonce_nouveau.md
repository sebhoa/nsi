Écrire une fonction `recherche_min` qui prend en paramètre un tableau `tableau` non vide et nom trié de nombres, et qui renvoie l'indice de la première occurrence du minimum de ce tableau. Les
tableaux seront représentés sous forme de listes Python.

Exemples :
```python
>>> recherche_min([5])
0
>>> recherche_min([2, 4, 1])
2
>>> recherche_min([5, 3, 2, 2, 4])
2
```
