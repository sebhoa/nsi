Beaucoup à reprendre sans cet exercice d'après F. Chambon :

- mauvais nom de fonction et très mauvais noms de variable
- fautes d’orthographe et imprécisions et pourtant sujet trop long
- contournements (contorsionnements) pour éviter d’écrire taille et bits l’un après l’autre
- renvoyer la longueur de la liste n’a aucun intérêt en Python ; on n’est pas en C
- exemple très mal choisi, c’est un palindrome en binaire

Source : [commentaires à propos du 07.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/14).

Les modifications sont directement inspirées de https://ens-fr.gitlab.io/nsi-pratique/ex1/07/sujet/

_A noter_ : l'exercice est très similaire au 15.2. Il est étonnant soit proposé en exercice 1 et l'autre en 2. Une harmonisation s'impose. 

