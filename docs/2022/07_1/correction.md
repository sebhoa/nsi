```python linenums='1'
def conversion_binaire(n):
    bits = [n%2]
    n = n // 2
    while n != 0:
        bits.append(n%2)
        n = n // 2
    bits.reverse()
    return bits
```