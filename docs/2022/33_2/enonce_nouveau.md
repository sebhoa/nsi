**Version avec peu de modifs**

La fonction `tri_insertion` suivante prend en argument une liste `L` et trie cette liste en
utilisant la méthode du tri par insertion. Compléter cette fonction pour qu'elle réponde à la
spécification demandée.

```python linenums='1'
def tri_insertion(tab):
    n = len(tab)

    # cas du tableau vide
    if ...:
        return tab

    for j in range(1, n):
        e = tab[j]
        i = j

        # A l'étape j, le sous-tableau tab[0,j-1] est trié
        # et on insère tab[j] dans ce sous-tableau en déterminant
        # le plus petit i tel que 0 <= i <= j et tab[i-1] > tab[j].
        while i > 0 and tab[i-1] > ...:
            i = ...

        # si i != j, on décale le sous tableau tab[i,j-1] d’un cran
        # vers la droite et on place tab[j] en position i
        if i != j:
            for k in range(j, i, ...):
                tab[k] = tab[...]
            tab[i] = ...
    return tab
```

Exemples :
```python
>>> tri_insertion([2, 5, -1, 7, 0, 28])
[-1, 0, 2, 5, 7, 28]
>>> tri_insertion([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

**Version avec plus de modifs**
La fonction `tri_insertion` suivante prend en argument un tableau `tab` et trie ce tableau en
utilisant la méthode du tri par insertion. Compléter cette fonction pour qu'elle réponde à la
spécification demandée.
```python linenums='1'
def tri_insertion(tab):
    n = len(tab)
    for i in range(1, n):
        j = ...
        valeur_a_inserer = tab[...]
        while j > ... and valeur_a_inserer < tab[...]:
            tab[j] = tab[j-1]
            j = ...
        tab[j] = ...
    return tab
```

Exemples :
```python
>>> tri_insertion([2, 5, -1, 7, 0, 28])
[-1, 0, 2, 5, 7, 28]
>>> tri_insertion([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

