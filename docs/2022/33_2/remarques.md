Commentaires de R. Janvier :

Encore une fois, des `return` qui n'ont aucun intérêt. 
On traite le cas vide à part alors qu'on ne rentre juste pas dans la boucle, donc aucun intérêt. 
Il y a un problème d'indentation des commentaires qui fait croire que la boucle est finie plus tôt que prévue. 
On fait 2 parcours pour trouver la position où insérer la valeur. 
Les variables ont des noms trop courts. 
Comment se souvenir que `e` sert à conserver la valeur de `L[j]` pour la remettre à la fin. Perso, j'ai dû chercher. 
On demande de rajouter le `-1` dans un `range` pour faire un compte à rebours. Pas sûr que ce soit au programme. 
Mal foutu et compliqué. Les élèves vont peut-être trouver en tatonnant, mais est-ce que c'est ce qu'on veut encourager ? 
Franchement, ce problème se règle en 6 lignes. 
Pourquoi en mettre 2 fois plus en étant moins clair ? 
Problématique.

[commentaire à propos du 33.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62#exercice-2-tri-par-insertion-11)

