Commentaires de R. Janvier :

Est-ce que ce n'est pas trop dur pour un exercice 1 ? 
Le sujet propose d'utiliser les puissances de 2. Sauf que je pense que mes élèves non matheux sont incapables de trouver la formule de l'exposant pour la puissance, même si elle est triviale. 
Surtout que je n'ai utilisé que la méthode de Horner avec eux (ok, j'ai dû faire 1 fois les puissances en début de 1e), mais je ne pense pas que les non matheux, encore eux, puisse retrouver la méthode tous seuls. 
Cela devrait être un exercice 2, avec du code à compléter, même si, il est vrai que ça fait une fonction très courte.
Au moins, comme on utilise une liste d'entier, on évite les conversions entre `str` et `int`. 
Il manque des tests avec 0 ou 1. 
Et on pourrait mettre `tab` au lieu de `T`.
Problématique.

[commentaire à propos du 33.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/62#exercice-1-conversion-binaire-vers-dcimal-10)
