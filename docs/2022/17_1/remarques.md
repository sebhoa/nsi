Q. Konieczko trouve cet exercice _bizarre_ mais ne lui trouve pas de défaut majeur.

Source : [commentaires à propos du 17.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12)

Effectivement, le _bizarre_ vient probablement qu'au début on pense à une manipulation sur les chaîne de caractères, puis dans l'énoncé on nous annonce qu'il suffit en fait de compter le nombre d'espaces. 

L'exercice est donc en fait une sorte de nombre d'occurences. Tout va bien. Il faudrait ajouter quelques exemples.