**Version avec peu de modifs**

Le but de l'exercice est de compléter une fonction qui détermine si une valeur est présente
dans un tableau de valeurs triées dans l'ordre croissant.

L'algorithme traite le cas du tableau vide.

L'algorithme est écrit pour que la recherche dichotomique ne se fasse que dans le cas où
la valeur est comprise entre les valeurs extrêmes du tableau.

On distingue les trois cas qui renvoient `False` en renvoyant `False, 1` , `False, 2` et
`False, 3`.

Compléter l'algorithme de dichotomie donné ci-après.

```python linenums='1'
def dichotomie(tab, val):
    """
        tab : tableau trié dans l’ordre croissant
        val : nombre entier
        La fonction renvoie True si tab contient val et False sinon
    """
    # cas du tableau vide
    if ...:
        return False, 1
    # cas où val n'est pas compris entre les valeurs extrêmes
    if (val < tab[0]) or ...:
        return False, 2
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = ...
        if val == tab[m]:
            return ...
        if val > tab[m]:
            debut = m + 1
        else:
            fin = ...
    return ...
```

Exemples :

```python
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],28)
True
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],27)
(False, 3)
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],1)
(False, 2)
>>> dichotomie([], 28)
(False, 1)
```

**Version avec un changement plus important**

Le but de l'exercice est de compléter une fonction qui détermine si une valeur est présente
dans un tableau de valeurs triées dans l'ordre croissant.

L'algorithme traite le cas du tableau vide.

L'algorithme renvoie un couple de valeurs composé d'un booléen indiquant si la valeur
a été trouvée, ainsi qu'un entier indiquant le nombre d'étapes nécessaires pour
arriver à cette réponse.

Compléter l'algorithme de dichotomie donné ci-après.

```python linenums='1'
def dichotomie(tab, val):
    """
        tab : tableau trie dans l'ordre croissant
        val : nombre entier
        La fonction renvoie True si tab contient val et False sinon
    """
    nb_etapes = ...
    debut = ...
    fin = len(tab) - 1
    while debut <= fin:
        nb_etapes = ...
        m = ...
        if val == tab[m]:
            return ...
        if val > tab[m]:
            debut = ...
        else:
            fin = ...
    return ...
```

Exemples :

```python
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],28)
(True, 4)
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],27)
(False, 4)
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],23)
(True, 1)
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],1)
(False, 3)
>>> dichotomie([], 28)
(False, 0)
```

