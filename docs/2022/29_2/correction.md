```python linenums='1'
def meilleures_notes(tab_eleves, tab_notes):
    note_maxi = 0
    liste_maxi = []
    for i in range(len(tab_eleves)):
        eleve, note = tab_eleves[i], tab_notes[i]
        if note == note_maxi:
            liste_maxi.append(eleve)
        if note > note_maxi:
            note_maxi = note
            liste_maxi = [eleve]
    return note_maxi, liste_maxi
```