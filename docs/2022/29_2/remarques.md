N. Bonnin pointe l'essentiel de ce qui ne va pas dans cet exercice :

- la fonction travaille sans paramètre et sur 2 variables globales.

Retrouvez l'ensemble de ses [commentaires à propos du 29.2](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/26)

