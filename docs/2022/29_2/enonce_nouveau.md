
On stocke dans deux tableaux, des élèves (identifiés pour simplifier par une seule lettre) et leurs notes.

La fonction `meilleures_notes` prend les deux tableaux, celui des élèves en premier paramètre et celui des notes en deuxième et renvoie la note maximale qui a été attribuée, ainsi que la liste des élèves ayant obtenus cette note.

Compléter le code Python de la fonction `meilleures_notes` ci-dessous.

```python linenums='1'

def meilleures_notes(tab_eleves, tab_notes):
    note_maxi = 0
    liste_maxi = ...
    for i in range(...):
        eleve = tab_eleves[i]
        note = tab_notes[i]
        if ... == note_maxi:
            liste_maxi.append(...)
        if ... > ...:
            note_maxi = ...
            liste_maxi = [...]
    return note_maxi, liste_maxi
```

Exemples :

```python
>>> eleves = ['a','b','c','d','e','f','g','h','i','j']
>>> notes = [1, 40, 80, 60, 58, 80, 75, 80, 60, 24]
>>> meilleures_notes(eleves, notes)
(80, ['c', 'f', 'h'])
```