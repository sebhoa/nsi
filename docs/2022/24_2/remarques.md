L'exercice voulait utiliser une Pile. Malheureusement l'implémentation de la Pile souffre du syndrôme du paramètre par défaut mutable. D'autre part, avec un seul type de caractère (la parenthèse) la Pile est inutile, il suffit de compter :

- on compte +1 pour une parenthèse ouvrante, -1 pour une fermante
- si au cours du comptage on tombe sur -1 on renvoie `False` : le parenthésage n'est pas correct
- à la fin on renvoie `True` si le compteur est à 0 et `False` sinon

On va donc proposé deux versions modifiées :

- la première garde seulement les parenthèses et supprime l'utilisation de la Pile
- la deuxième rajoute les crochets (`[` et `]`) et les accolades (`{` et `}`}) et propose une implémentation correcte de la Pile.