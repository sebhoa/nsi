On dispose de chaînes de caractères contenant uniquement des parenthèses ouvrantes et
fermantes. 

Un parenthésage est correct si :

- le nombre de parenthèses ouvrantes de la chaîne est égal au nombre de parenthèses
fermantes.
- en parcourant la chaîne de gauche à droite, le nombre de parenthèses déjà ouvertes doit
être, à tout moment, supérieur ou égal au nombre de parenthèses déjà fermées.

- "((()())(()))" est un parenthésage correct. 
- les parenthésages "())(()" et "(())(()" sont, eux, incorrects.


On souhaite programmer une fonction `bien_parenthesee` qui prend en paramètre une chaîne `chaine` de
parenthèses et renvoie `True` si la chaîne est bien parenthésée et `False` sinon.

Le principe est le suivant :

- on compte +1 pour une parenthèse ouvrante, -1 pour une fermante
- si on est sur une parenthèse fermante alors que le compteur est à 0, on renvoie `False` : le parenthésage n'est pas correct
- à la fin on renvoie `True` si le compteur est à 0 et `False` sinon


```python linenums='1'
def parenthesage (ch):
    """Renvoie True si la chaîne ch est bien parenthésée et False sinon"""
    compteur = ...
    for c in ch:
        if c == ...:
            compteur += 1
        elif c == ...:
            if ...:
                return ...
            else:
                ...
    return ...

assert parenthesage("((()())(()))") == True
assert parenthesage("())(()") == False
assert parenthesage("(())(()") == False
```


**Version nécessitant une Pile** (beaucoup plus difficile, probablement trop)

On dispose de chaînes de caractères contenant :
- des parenthèses ouvrantes et fermantes
- des crochets ouvrants et fermants
- des accolades ouvrantes et fermantes

On appellera _délimiteur_ une parenthèse, un crochet ou une accolade. Un parenthésage est correct si :

- pour chaque délimiteur le nombre de fermants coïncide avec le nombre d'ouvrants
- en parcourant la chaîne de gauche à droite, pour chaque délimiteur, le nombre d'ouvrant doit
être, à tout moment, supérieur ou égal au nombre de fermants.
- les différents délimiteurs ne doivent pas s'entre-croiser : `[(])` est incorrect.


- "([()[]]{()})" est un parenthésage correct. 
- les parenthésages "{}[(])" (entre-croisement) et "[[]]({})]" (crochet fermant en trop à la fin) sont, eux, incorrects.


On souhaite programmer une fonction `bien_parenthesee` qui prend en paramètre une chaîne `chaine` de
délimiteurs et renvoie `True` si la chaîne est bien parenthésée et `False` sinon.

Le principe est le suivant en se servant d'une Pile (dont l'implémentation vous est donnée) :

- on parcourt les caractères de la chaîne, si c'est un délimiteur ouvrant on l'empile
- si c'est un délimiteur fermant :
    - si la Pile est vide, on renvoie `False` : le parenthésage est incorrect
    - si la Pile n'est pas vide mais que le caractère dépilé n'est pas le délimiteur ouvrant de celui qu'on a : on renvoie `False`
- à la fin du parcourt : si la Pile est vide on renvoie `True` sinon on renvoie `False`

On pourra se servir d'un dictionnaire pour faire la correspondance entre le délimiteur ouvrant et le fermant.

```python linenums='1'
DELIMITEURS = {'(': ')', '{': '}', '[': ']'}

class Pile:
    """ Classe définissant une pile """
    def __init__(self):
        self.valeurs = []

    def est_vide(self):
        """Renvoie True si la pile est vide, False sinon"""
        return self.valeurs == []

    def empiler(self, c):
        """Place l’élément c au sommet de la pile"""
        self.valeurs.append(c)

    def depiler(self):
        """Supprime l’élément placé au sommet de la pile, à condition qu’elle soit non vide"""
        if self.est_vide() == False:
            return self.valeurs.pop()

def bien_parenthesee(ch):
    """Renvoie True si la chaîne ch est bien parenthésée et False sinon"""
    p = Pile()
    for c in ch:
        if c in DELIMITEUR:
            ...
        elif p.est_vide():
            return ...
        else:
            dernier = ...
            if dernier != ...:
                return ... 
    return ...


assert bien_parenthesee("([()[]]{()})") == True
assert bien_parenthesee("{}[(])") == False
assert bien_parenthesee("[[]]({})]") == False
```

On peut raccourcir un peu :

```python
def bien_parenthesee(ch):
    """Renvoie True si la chaîne ch est bien parenthésée et False sinon"""
    p = Pile()
    for c in ch:
        if c in DELIMITEUR:
            ...
        elif p.est_vide() or c != DELIMITEUR[...]:
            return ...
    return ...
```