# Table ronde

## Invité.e.s

- **S. Chane-Lune**
    - Capéssienne en Mathématiques
    - Enseignante au collège dans cette discipline
    - Enseignante de la spécialité NSI au lycée Le Verger
- **F. Fabre Ferber**, doctorant en Informatique au LIM
- **R. Girard**
    - Formation d'Informaticien à ??
    - Thèse à l'Université de la Réunion, spécialité : les logiques floues
    - Enseignant-chercheur en Informatique, spécialisé en Algorithmique et Structures de données
    - Directeur du département Informatique de l'UFR Sciences et Technologies, 2e mandat (3e ?) 
- **D. Grondin**
    - Ingénieur en Informatique (2014)
    - Développeur polyvalent (2011) différentes technos (serverless, la blockchain, etc) et méthodes de conception en matière de services numériques
    - sécurité informatique, divertissement, santé, Internet des objets en outre en y créant des applications desktop, mobile et web. J’ai contribué à quelques initiatives open source. Aujourd’hui, je suis entrepreneur dans le but d’initier et d’accompagner les porteurs de projet dans le numérique qu’ils soient au stage de la création, lancement ou la mise à l’échelle de leurs produits ou services numériques.
- **J. Mauras**
    - Président fondateur de ExoData

- **P.-U. Tournoux**, 
    - Maître de conférence à l’Université de la Réunion dans la spécialité Informatique et Télécommunication de l’ESIROI
    - Chercheur au LIM, 
    - spécialisé dans les réseaux mobiles et les problématiques de respect de la vie privée dans ces réseaux. 
    - Co-diplômé du Master réseau de UPMC et de l’ENST Paris en 2008, 
    - Thèse à l’école d’ingénieur ISAE et au LAAS-CNRS (Laboratoire d'analyse et d'architecture des systèmes)
    - Début carrière dans le groupe Network Research Group du National ICT Australia (aujourd’hui DATA61 du CSIRO) où mes travaux ont porté sur les problématiques de vie privée dans les réseaux informatiques


## Questions

- Pour Sophie et Frédérick
    > Vous avez en commun d'avoir suivi, avec quelques années d'écart, une Licence d'Informatique, puis un BAC+5 (maîtrise DEA pour Sophie, Master pour Frédérick), est-ce que l'Informatique était ce que vous souhaitiez faire dès le lycée ?